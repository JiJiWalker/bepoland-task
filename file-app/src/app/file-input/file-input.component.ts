import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { CustomFile } from '../shared/customFile.model';
// import * as EXIF from '../../../node_modules/exif-js';

declare var EXIF: any;

@Component({
  selector: 'app-file-input',
  templateUrl: './file-input.component.html',
  styleUrls: ['./file-input.component.css']
})
export class FileInputComponent implements OnInit {
  @Output() fileDetails = new EventEmitter<CustomFile>();
  @ViewChild('imgTag', {static: true}) imgEl: ElementRef;
  imageDetails: any;
  // CustomFile file properties
  fileSize: number;
  fileName: string;
  fileType = '';
  data: any;
  errorMsg: string;
  invalid = false;

  constructor() { }

  ngOnInit() {
  }

  onImageSelect(event: any) {
    if (event.target.files && event.target.files[0]) {
      const myFile = event.target.files[0] as File;
      this.fillFileDetails(myFile);
      console.log(myFile);
      if (this.validationFile(myFile)) {
        const reader = new FileReader();
        reader.readAsDataURL(myFile);
        reader.onload = (eventL) => {
        this.data = (eventL.target as FileReader).result; // without brackets and casting it was genereting error
        };
      }
    }
  }

  onImageLoad() {
    let allMetaData: any;
    let isGPS = false;
    this.invalid = false;
    EXIF.getData(this.imgEl.nativeElement as HTMLImageElement, function() {
      allMetaData = EXIF.getAllTags(this);
      if (allMetaData.hasOwnProperty('GPSLatitude') && allMetaData.hasOwnProperty('GPSLongitude')) {
        isGPS = true;
      }
      delete this.exifdata; // https://github.com/exif-js/exif-js/issues/163 for referenc
    });
    if (isGPS) {
      this.fileSize = Math.round(this.fileSize / 1024);
      this.fileDetails.emit(
        new CustomFile(this.data, this.fileName, this.fileType, this.fileSize, allMetaData.GPSLatitude, allMetaData.GPSLongitude)
      );
    } else {
      this.invalid = true;
      this.errorMsg = 'Image does not contains GPS information';
    }
  }

  private fillFileDetails(file: File) {
    this.fileName = file.name;
    this.fileType = file.name.split('.').pop();
    this.fileSize = file.size;
  }

  private validationFile(f: File) {
    this.invalid = false;
    if (f.size > 1048576) {
      this.errorMsg = 'Too large file (max 1MB)';
      this.invalid = true;
      return false;
    } else if (f.name.split('.').pop() !== 'jpg' && f.name.split('.').pop() !== 'JPG') {
      this.errorMsg = 'File type is not supported';
      this.invalid = true;
      return false;
    } else {
      return true;
    }
  }

}

export class CustomFile {
    public thumbnail: string | ArrayBuffer;
    public fileName: string;
    public fileExtension: string;
    public fileSize: number;
    public latitude: number[];
    public longitude: number[];

    constructor(thumb: string, name: string, ext: string, size: number, lat: number[], long: number[]) {
        this.thumbnail = thumb;
        this.fileName = name;
        this.fileExtension = ext;
        this.fileSize = size;
        this.latitude = lat;
        this.longitude = long;
    }
}

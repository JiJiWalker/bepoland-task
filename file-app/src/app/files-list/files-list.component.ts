import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CustomFile } from '../shared/customFile.model';

@Component({
  selector: 'app-files-list',
  templateUrl: './files-list.component.html',
  styleUrls: ['./files-list.component.css']
})
export class FilesListComponent implements OnInit {
  // @Input() fileIn: File;
  @Input() fileList: CustomFile[];
  @Output() deletedIndex = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  deleteRecord(i) {
    console.log(i);
    this.fileList.splice(i, 1);
    this.deletedIndex.emit(i);
  }
}

import { Component, OnInit, Input } from '@angular/core';
import { CustomFile } from 'src/app/shared/customFile.model';

@Component({
  selector: 'app-file-list-item',
  templateUrl: './file-list-item.component.html',
  styleUrls: ['./file-list-item.component.css']
})
export class FileListItemComponent implements OnInit {
  @Input() fileItem: CustomFile;

  constructor() { }

  ngOnInit() {
  }

  onBtn() {
    console.log(this.fileItem);
  }

}

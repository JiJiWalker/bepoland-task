import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FileInputComponent } from './file-input/file-input.component';
import { FilesListComponent } from './files-list/files-list.component';
import { FileListItemComponent } from './files-list/file-list-item/file-list-item.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MapComponent } from './map/map.component';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
    AppComponent,
    FileInputComponent,
    FilesListComponent,
    FileListItemComponent,
    HeaderComponent,
    FooterComponent,
    MapComponent,
  ],
  imports: [
    BrowserModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBOdPZCqfTdb3txmISXg-GSedPKP9dvSMs'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

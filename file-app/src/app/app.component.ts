import { Component } from '@angular/core';
import { CustomFile } from './shared/customFile.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  fileSelected: CustomFile;
  fileListSelected: CustomFile[] = [];
  markersList: any[] = [];
  option = 'file';

  filePicked(event: CustomFile) {
    this.fileSelected = event;
    this.fileListSelected.push(event);
    this.markersList.push({lat: Number(event.latitude[2].toString()), lng: Number(event.longitude[2].toString())});
  }

  showOption(event: string) {
    this.option = event;
  }

  markerToDelete(index) {
    this.markersList.splice(index, 1);
  }
}

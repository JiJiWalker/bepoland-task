import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  @Input() markers: any[];
  lat: number;
  lng: number;

  // markers1 = [
  //   { lat: 22.33159, lng: 105.63233, alpha: 1 },
  //   { lat: 7.92658, lng: -12.05228, alpha: 1 },
  //   { lat: 48.75606, lng: -118.859, alpha: 1 },
  //   { lat: 5.19334, lng: -67.03352, alpha: 1 },
  //   { lat: 12.09407, lng: 26.31618, alpha: 1 },
  //   { lat: 47.92393, lng: 78.58339, alpha: 1 }
  // ];

  constructor() { }

  ngOnInit() {
  }

}

# FileApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.19.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Instruction

1. To run project, first you must ensoure that your development environment includes Node.js and npm package manager.
	- to check this, run `node -v` in your terminal/console windows. If you get an error, go to [nodejs.org](https://nodejs.org/en/) to setup Node.js
	
2. Check if you have npm package manager. Run `npm -v` in terminal/console. npm pachage should be provided with Node.js by default
3. Angular CLI. To create, generate and run Angular application, you will need Angular CLI. 
	- first, install Angular CLI.Run command `npm install -g @angular/cli`;
	- secondly, clone this project to your local harddrive following clone link from source tab or [tutorial](https://www.toolsqa.com/git/git-clone/) (you need to have git [setuped](https://help.github.com/en/github/getting-started-with-github/set-up-git) for this)
	- now, when you finally have project on your local drive, go to terminal, go to the workspace/app-name (../file-app) and run `ng serve` command. It should launch server with builded application.
	- when server started, app can be found at `http://localhost:4200/`